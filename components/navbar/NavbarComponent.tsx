'use client'

import {Avatar, Dropdown, Navbar} from "flowbite-react";
import {BsCart} from "react-icons/bs";
import {useRouter} from "next/navigation";
const NavbarComponent = () => {
    const router = useRouter();
    return(
        <Navbar fluid className="container mx-auto mb-8">
            <Navbar.Brand href="/">
                <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">My Shop</span>
            </Navbar.Brand>
            <div className="flex md:order-2">
                <button className="text-2xl flex justify-center items-center p-2 mr-2 bg-gray-300 rounded-full"
                        onClick={() => router.push("/cart")}>
                    <BsCart/>
                </button>
                <Dropdown
                    arrowIcon={false}
                    inline
                    label={
                        <Avatar alt="User settings" img="https://flowbite.com/docs/images/people/profile-picture-5.jpg" rounded />
                    }
                >
                    <Dropdown.Header>
                        <span className="block text-sm">Bonnie Green</span>
                        <span className="block truncate text-sm font-medium">name@flowbite.com</span>
                    </Dropdown.Header>
                    <Dropdown.Item>Dashboard</Dropdown.Item>
                    <Dropdown.Item>Settings</Dropdown.Item>
                    <Dropdown.Item>Earnings</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Sign out</Dropdown.Item>
                </Dropdown>
                <Navbar.Toggle />
            </div>
            <Navbar.Collapse>
                <Navbar.Link href="/" active>
                    Home
                </Navbar.Link>
                <Navbar.Link href="/products">Products</Navbar.Link>
                <Navbar.Link href="#">My Products</Navbar.Link>
                <Navbar.Link href="/about-us">About Us</Navbar.Link>
                <Navbar.Link href="/contact">Contact Us</Navbar.Link>
                <Navbar.Link href="/privacy">Privacy & Policy</Navbar.Link>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default NavbarComponent;