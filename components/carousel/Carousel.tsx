import React, { useState, useEffect } from 'react';

interface CarouselProps {
    images: string[];
    autoplayInterval?: number; // Optional prop for autoplay interval in milliseconds
}

const Carousel: React.FC<CarouselProps> = ({ images, autoplayInterval = 3000 }) => {
    const [currentIndex, setCurrentIndex] = useState(0);

    useEffect(() => {
        const intervalId = setInterval(goToNextSlide, autoplayInterval);
        return () => clearInterval(intervalId);
    }, [currentIndex]); // Restart timer when currentIndex changes

    const goToNextSlide = () => {
        setCurrentIndex((prevIndex) => (prevIndex === images.length - 1 ? 0 : prevIndex + 1));
    };

    const goToPrevSlide = () => {
        setCurrentIndex((prevIndex) => (prevIndex === 0 ? images.length - 1 : prevIndex - 1));
    };

    return (
        <div className="carousel">
            <button onClick={goToPrevSlide}>Previous</button>
            <img src={images[currentIndex]} alt={`Slide ${currentIndex}`} />
            <button onClick={goToNextSlide}>Next</button>
        </div>
    );
};

export default Carousel;
