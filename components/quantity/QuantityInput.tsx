'use client'

import {useState} from "react";
import {BiMinus, BiPlus} from "react-icons/bi";

const QuantityInput = () => {

    const [quantity, setQuantity] = useState(1); // State to manage quantity

    // Function to handle decreasing quantity
    const handleDecrease = () => {
        if (quantity > 1) {
            setQuantity(quantity - 1);
        }
    };

    // Function to handle increasing quantity
    const handleIncrease = () => {
        setQuantity(quantity + 1);
    };

    return (
        <div>
            <label htmlFor="Quantity" className="sr-only"> Quantity </label>

            <div className="flex items-center gap-2">
                <button type="button"
                        className="size-10 leading-10 text-gray-600 transition hover:opacity-75 flex justify-center items-center"
                        onClick={handleDecrease}>
                    <BiMinus/>
                </button>

                <input
                    type="number"
                    id="Quantity"
                    value={quantity} // Bind input value to quantity state
                    className="h-10 w-16 rounded border-gray-200 text-center [-moz-appearance:_textfield] sm:text-sm [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none"
                />

                <button type="button"
                        className="size-10 leading-10 text-gray-600 transition hover:opacity-75 flex justify-center items-center"
                        onClick={handleIncrease}>
                    <BiPlus/>
                </button>
            </div>
        </div>
    )
}
export default QuantityInput;