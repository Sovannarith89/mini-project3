import {BsEyeFill, BsHeart} from "react-icons/bs";
import { useDispatch } from 'react-redux';
import {addToCart} from "@/lib/action";

type ProductProps = {
    name: string;
    image: string;
    price: number;
    seller: string;
    category: string;
    onClick: () => void;
};

export default function CardProduct({name,image,price,onClick}: ProductProps){

    // const dispatch = useDispatch();
    //
    // const handleAddToCart = () => {
    //     dispatch(addToCart({ id, name, image, price }));
    // };

    return (
        <div className="group block overflow-hidden w-64 cursor-pointer" >
            <img
                onClick={onClick}
                src={image}
                alt=""
                className="h-40 w-full object-cover overflow-hidden transition duration-500 group-hover:scale-105 sm:h-40"
            />
            <div className=" border border-gray-100 bg-white p-4">
                <div className="flex gap-2 mb-2">
                    <span className="badge">stock ready</span>
                    <span className="badge">official store</span>
                </div>
                <span className="whitespace-nowrap bg-yellow-400 px-3 py-1.5 text-xs font-medium"> New </span>
                <h3 className="mt-4 text-lg font-medium text-gray-900 line-clamp-1">{name}</h3>

                <p className="mt-1.5 text-sm text-gray-700">${price}</p>

                <form className="mt-4 flex gap-2">
                    <button
                        className="button-primary bg-yellow-500/80 hover:bg-yellow-500/90" onClick={onClick}
                    >
                        Add to Cart
                    </button>
                    <button className="button-icon" onClick={onClick}>
                        <BsHeart/>
                    </button>
                    <button className="button-icon" onClick={onClick}>
                        <BsEyeFill/>
                    </button>
                </form>
            </div>
        </div>
    )
}