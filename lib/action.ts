
export const ADD_TO_CART = 'ADD_TO_CART';

export const addToCart = (product: Product) => {
    return {
        type: ADD_TO_CART,
        payload: product
    };
};

type Product = {
    id: string;
    name: string;
    image: string;
    price: number;
};
