'use client'

import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const CreateProductForm = () => {
    const initialValues = {
        name: '',
        description: '',
        price: 0,
        quantity: 0,
        image: null,
    };

    const validationSchema = Yup.object({
        name: Yup.string().required('Required'),
        description: Yup.string().required('Required'),
        price: Yup.number().required('Required').min(0, 'Price must be greater than or equal to 0'),
        quantity: Yup.number().required('Required').min(0, 'Quantity must be greater than or equal to 0'),
        image: Yup.mixed().required('Image is required'),
    });

    const handleSubmit = (values: any) => {
        // Handle form submission, e.g., send data to backend
        console.log(values);
    };

    return (
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
            <Form className="max-w-md mx-auto p-6 bg-white shadow-md rounded-md">
                <div className="mb-4">
                    <label htmlFor="name" className="block text-sm font-medium text-gray-700">Product Name</label>
                    <Field type="text" id="name" name="name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                    <ErrorMessage name="name" component="div" className="text-red-500 text-sm mt-1" />
                </div>
                <div className="mb-4">
                    <label htmlFor="description" className="block text-sm font-medium text-gray-700">Description</label>
                    <Field type="text" id="description" name="description" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                    <ErrorMessage name="description" component="div" className="text-red-500 text-sm mt-1" />
                </div>
                <div className="mb-4">
                    <label htmlFor="price" className="block text-sm font-medium text-gray-700">Price ($)</label>
                    <Field type="number" id="price" name="price" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                    <ErrorMessage name="price" component="div" className="text-red-500 text-sm mt-1" />
                </div>
                <div className="mb-4">
                    <label htmlFor="quantity" className="block text-sm font-medium text-gray-700">Quantity</label>
                    <Field type="number" id="quantity" name="quantity" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                    <ErrorMessage name="quantity" component="div" className="text-red-500 text-sm mt-1" />
                </div>
                <div className="mb-4">
                    <label htmlFor="image" className="block text-sm font-medium text-gray-700">Image</label>
                    <Field type="file" id="image" name="image" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                    <ErrorMessage name="image" component="div" className="text-red-500 text-sm mt-1" />
                </div>
                <button type="submit" className="w-full inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Submit
                </button>
            </Form>
        </Formik>
    );
};

export default CreateProductForm;