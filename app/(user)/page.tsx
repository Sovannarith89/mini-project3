'use client'

import CardComponent from "@/components/cards/CardComponent";
import {useEffect, useState} from "react";
import {useRouter} from "next/navigation";
import {BASE_URL} from "@/lib/constants";
import HeroComponent from "@/components/hero/HeroSection";
import FeatureProduct from "@/components/feature/FeatureProduct";
import StateComponent from "@/components/state/State";
import Collection from "@/components/collection/Collection";

export default function Home() {
    const [products, setProducts] = useState([])
    const router = useRouter();
    useEffect(() => {
        fetch(`${BASE_URL}/api/products/`)
            .then(res => res.json()).then(data => {
            setProducts(data.results)
        }).catch(err => console.log(err))
    }, [])
    return (
        <main className="container mx-auto">
            <HeroComponent/>
            <div className="mb-8">
                <h1 className="text-3xl font-semibold my-4">Latest Products</h1>
                <div className="flex flex-wrap gap-8 justify-center">
                    {products.map((product: any, index) => (
                        <CardComponent key={index}
                                       image={product.image}
                                       name={product.name}
                                       onClick={() => router.push(`/products/${product.id}`)}
                                       price={product.price} category={""} seller={""}/>))}
                </div>
            </div>
            <Collection/>
            <FeatureProduct/>
            <StateComponent/>
        </main>
    );
}
